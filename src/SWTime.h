#include "SWDefinitions.h"

typedef struct 
{
	bool bInitialized;

	bool bExistsHTImg;
	bool bExistsHTLyr;
	
	bool bSetHTImg;
	bool bSetHTLyr;
	
	bool bExistsHOImg;
	bool bExistsHOLyr;
	
	bool bSetHOImg;
	bool bSetHOLyr;
	
	bool bExistsMTImg;
	bool bExistsMTLyr;
	
	bool bSetMTImg;
	bool bSetMTLyr;
	
	bool bExistsMOImg;
	bool bExistsMOLyr;
	
	bool bSetMOImg;
	bool bSetMOLyr;
	
	bool bExistsPTImg;
	bool bExistsPTLyr;
	
	bool bSetPTImg;
	bool bSetPTLyr;
	
	bool bExistsDotImg;
	bool bExistsDotLyr;
	
	bool bSetDotImg;
	bool bSetDotLyr;
	
	bool bUpdatePTDigit;
	bool bUpdateDot;
	bool bUpdateHTDigit;
	bool bUpdateHODigit;
	bool bUpdateMTDigit;
	bool bUpdateMODigit;
	
} TimeStateVars;

typedef struct 
{		
	
	BitmapLayer* bmplyrTimePastTwelve;
	BitmapLayer* bmplyrTimeDot;
	BitmapLayer* bmplyrTimeHourTens;
	BitmapLayer* bmplyrTimeHourOnes;
	BitmapLayer* bmplyrTimeMinTens;
	BitmapLayer* bmplyrTimeMinOnes;
	
	/*
	BitmapLayer* bmplyrTimeHourTens;
	BitmapLayer* bmplyrTimeHourOnes;
	BitmapLayer* bmplyrTimeMinTens;
	BitmapLayer* bmplyrTimeMinOnes;
	BitmapLayer* bmplyrColon;
	*/
	GBitmap* imgTime[TOTAL_TIME_DIGITS];
	//GBitmap* imgTime_SMALL[TOTAL_TIME_DIGITS];
	
	//GBitmap* imgDOT;
	
	int iCurrentHour;
	int iPreviousHour;
	
	int iPreviousPastTwelveDigit;	
	int iPreviousHourTensDigit;
	int iPreviousHourOnesDigit;
	int iPreviousMinTensDigit;
	int iPreviousMinOnesDigit;	
	
	int iCurrentPastTwelveDigit;
	int iCurrentHourTensDigit;
	int iCurrentHourOnesDigit;
	int iCurrentMinTensDigit;
	int iCurrentMinOnesDigit;	

	GRect fraTimeFrame;
	GRect fraTimePastTwelveFrame;
	GRect fraTimeDotFrame;
	GRect fraTimeHourTensFrame;
	GRect fraTimeHourOnesFrame;
	GRect fraTimeMinTensFrame;
	GRect fraTimeMinOnesFrame;
	
	TimeStateVars State;

	//struct tm* tTime;
} SWTIME;

extern SWTIME SWTime;

void SWTIME_INIT(SWTIME* SWTime);
void SWTIME_DEINIT(SWTIME* SWTime);
void GetSetCurrentTime(SWTIME* SWTime, Layer* layer);
void GetCurrentTime(SWTIME* SWTime);
void SetCurrentTime(SWTIME* SWTime, Layer* layer);
void RemoveAndDeIntTime(SWTIME* SWTime);
void RemoveAndDeIntChangingTime(SWTIME* SWTime);
void SetTimeImages(SWTIME* SWTime, Layer* layer);
