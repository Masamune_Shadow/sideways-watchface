#include "SWWatchface.h"

extern SWWATCHFACE SWWatchface;

void CreateBackground()
{
	if (!SWWatchface.State.bSetBackground)
	{
		SWWatchface.lyrSWWatchface = bitmap_layer_create(SWWatchface.fraBackground);
		SWWatchface.imgBackground = gbitmap_create_with_resource(RESOURCE_ID_IMAGE_BACKGROUND);
		bitmap_layer_set_bitmap(SWWatchface.lyrSWWatchface, SWWatchface.imgBackground);
		SWWatchface.State.bSetBackground = true;
	}
}
	
void SetupWatchface(struct tm* t, Window* window)
{
	if (!SWWatchface.State.bExists && !SWWatchface.State.bInitialized)
	{
		SWWatchface.State.bInitializing = true;
		SWWatchface.State.bExists = true;
		SWWatchface.fraBackground = WATCH_FRA_BACKGROUND;
		CreateBackground();
		SWTIME_INIT(&SWWatchface.SWTime);
		GetSetCurrentTime(&SWWatchface.SWTime,bitmap_layer_get_layer(SWWatchface.lyrSWWatchface));
		
		SWDATE_INIT(&SWWatchface.SWDate);	
		
		SWWatchface.State.bInitializing = false;
		SWWatchface.State.bInitialized = true;
		SWWatchface.SWTime.iPreviousHour = SWWatchface.SWTime.iCurrentHour;
					
		layer_add_child(window_get_root_layer(window), bitmap_layer_get_layer(SWWatchface.lyrSWWatchface));
	}	
	else
	{
	//Second chance stuff

		if (!SWWatchface.SWTime.State.bInitialized)
		{
			//SWTIME_INIT(&SWWatchface.SWTime);
			SWTIME_INIT(&SWWatchface.SWTime);
			GetSetCurrentTime(&SWWatchface.SWTime,bitmap_layer_get_layer(SWWatchface.lyrSWWatchface));
		}
		
		if (!SWWatchface.SWDate.State.bInitialized)
		{
			SWDATE_INIT(&SWWatchface.SWDate);	
		}
	}	
	
	//Set the stuff
	if (SWWatchface.SWTime.State.bInitialized)
	{
		GetSetCurrentTime(&SWWatchface.SWTime,bitmap_layer_get_layer(SWWatchface.lyrSWWatchface));
	}
	
	if (SWWatchface.SWDate.State.bInitialized)
	{
		SetCurrentDateAndDateWord(&SWWatchface.SWDate,bitmap_layer_get_layer(SWWatchface.lyrSWWatchface));
	}
	
	
	SWWatchface.SWDate.iCurrentDay =  t->tm_mday;
	//SWWatchface.SWDate.iPreviousDay =  t->tm_mday;
	layer_set_hidden(bitmap_layer_get_layer(SWWatchface.lyrSWWatchface), false); //coming back from a DTDT or WTWT

}

void RemoveAndDeInt(char* cToRnD)
{
	bool bAll = false;
	
	if (strcmp(cToRnD,ALL) == 0)
	 	bAll = true;
	
	if (strcmp(cToRnD,TIME) == 0 || bAll)
		RemoveAndDeIntTime(&SWWatchface.SWTime);	
	if (strcmp(cToRnD,DATE) == 0 || bAll)
		RemoveAndDeIntDate(&SWWatchface.SWDate);
		
	if (strcmp(cToRnD,WATCH) == 0 || bAll)
	{
		if (SWWatchface.State.bSetBackground) 
		{
			layer_remove_from_parent(bitmap_layer_get_layer(SWWatchface.lyrSWWatchface));
			bitmap_layer_destroy(SWWatchface.lyrSWWatchface);
			gbitmap_destroy(SWWatchface.imgBackground);
			SWWatchface.State.bSetBackground = false;			
		}
	}
}

void SWWATCHFACE_DEINIT(char* cToRnD)
{
	bool bAll = false;
	
	if (strcmp(cToRnD,ALL) == 0)
	 	bAll = true;
	 	
	if (strcmp(cToRnD,TIME) == 0 || bAll)
		SWTIME_DEINIT(&SWWatchface.SWTime);
	if (strcmp(cToRnD,DATE) == 0 || bAll)
		SWDATE_DEINIT(&SWWatchface.SWDate);
		
	if (bAll)
	{
		SWWatchface.State.bInitialized = false;
		SWWatchface.State.bExists = false;
	}
}
