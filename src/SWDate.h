#include "SWDefinitions.h"

typedef struct 
{
	bool bInitialized;

	bool bExistsDTImg;
	bool bExistsDTLyr;
	
	bool bSetDTImg;
	bool bSetDTLyr;
	
	bool bExistsDOImg;
	bool bExistsDOLyr;
	
	bool bSetDOImg;
	bool bSetDOLyr;
	
	bool bExistsMTImg;
	bool bExistsMTLyr;
	
	bool bSetMTImg;
	bool bSetMTLyr;
	
	bool bExistsMOImg;
	bool bExistsMOLyr;
	
	bool bSetMOImg;
	bool bSetMOLyr;
	
	bool bExistsDWImg;
	bool bExistsDWLyr;
	
	bool bSetDWImg;
	bool bSetDWLyr;
	
	bool bUpdateDWDigit;
	bool bUpdateDTDigit;
	bool bUpdateDODigit;
	bool bUpdateMTDigit;
	bool bUpdateMODigit;
	
} DateStateVars;

typedef struct 
{		
	
	BitmapLayer* bmplyrDateWord;
	BitmapLayer* bmplyrDateMonthTens;
	BitmapLayer* bmplyrDateMonthOnes;
	BitmapLayer* bmplyrDateDayTens;
	BitmapLayer* bmplyrDateDayOnes;
	
	GBitmap* imgDate[TOTAL_DATE_DIGITS];
	GBitmap* imgDateWord;
	//GBitmap* imgDate_SMALL[TOTAL_DATE_DIGITS];
	
	//GBitmap* imgDOT;
	
	int iCurrentMonth;
	int iCurrentDay;
	
	int iPreviousDateWordDigit;	
	int iPreviousMonthTensDigit;
	int iPreviousMonthOnesDigit;
	int iPreviousDayTensDigit;
	int iPreviousDayOnesDigit;	
	
	int iCurrentDateWordDigit;
	int iCurrentMonthTensDigit;
	int iCurrentMonthOnesDigit;
	int iCurrentDayTensDigit;
	int iCurrentDayOnesDigit;	
	
	GRect fraDateFrame;
	GRect fraDateWordFrame;
	GRect fraDateMonthTensFrame;
	GRect fraDateMonthOnesFrame;
	GRect fraDateDayTensFrame;
	GRect fraDateDayOnesFrame;
	
	DateStateVars State;

	//struct tm* tDate;
} SWDATE;

extern SWDATE SWDate;

void SWDATE_INIT(SWDATE* SWDate);
void SWDATE_DEINIT(SWDATE* SWDate);
//void GetSetCurrentDate(SWDATE* SWDate, Layer* layer);
//void GetCurrentDate(SWDATE* SWDate);
void SetCurrentDateAndDateWord(SWDATE* SWDate, Layer* layer);
void RemoveAndDeIntDate(SWDATE* SWDate);

void SetDateImages(SWDATE* SWDate, Layer* layer);
